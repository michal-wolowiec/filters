<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Management</title>
    </head>
    <body>
    <% 
    if(request.getSession().getAttribute("login") == null){
    request.getSession().setAttribute("login", "anonymous");
    }
    if(request.getSession().getAttribute("login") != null){
    out.println("<p>You are logged in as: " + request.getSession().getAttribute("login") + "</h1>");
    }
    %>
        <h2>Main Page</h2>
        <p><a href="register">Register A New User</a></p>
        <p><a href="loginform">Login</a></p>
        <p><a href="profile">Your Profile</a></p>
        <p><a href="grantpremiumform">Grant Premium</a></p>
        <p><a href="showall">Show All Users</a></p>
        <p><a href="Premium.jsp">Premium Site(Premium Users Only)</a></p>
    </body>
</html>
