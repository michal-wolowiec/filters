package com.example.filter.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.filter.domain.User;
import com.example.filter.domain.Adress;

public class StorageService {
	
	private List<User> db = new ArrayList<User>();
	private List<Adress> ad = new ArrayList<Adress>();
	
	private Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	private PreparedStatement addUserStmt;
	private PreparedStatement getAllUsersStmt;
	private PreparedStatement searchPasswordStmt;
	private PreparedStatement searchUsernameStmt;
	private PreparedStatement grantPremiumStmt;
	private PreparedStatement getProfileStmt;
	private PreparedStatement getTypeStmt;
	
	private PreparedStatement addAdressStmt;
	private PreparedStatement getAdressesStmt;
	private PreparedStatement checkAdressStmt;
	private PreparedStatement editAdressStmt;
	private PreparedStatement getAdressStmt;
	private PreparedStatement deleteAdressStmt;
	
	private Statement statement;
	
	public void connect(){
		
		try{
			Class.forName("org.hsqldb.jdbc.JDBCDriver" );
		}catch(Exception e){
			System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
		     e.printStackTrace();
		}
		
		try{
			connection = DriverManager.getConnection(url, "SA", "");
			statement = connection.createStatement();
			
			//ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		
		addUserStmt = connection.prepareStatement("INSERT INTO User(username,password,email,type) VALUES(?, ?, ?, ?)");
		getAllUsersStmt = connection.prepareStatement("SELECT id, username,password,email,type FROM User");
		searchPasswordStmt = connection.prepareStatement("SELECT password FROM User WHERE username=?");
		searchUsernameStmt = connection.prepareStatement("SELECT username FROM User WHERE username=?");
		grantPremiumStmt = connection.prepareStatement("UPDATE User SET type='premium' WHERE username=?");
		getProfileStmt = connection.prepareStatement("SELECT id, username,password,email,type FROM User WHERE username=?");
		getTypeStmt = connection.prepareStatement("SELECT type FROM User WHERE username=?");
		
		addAdressStmt = connection.prepareStatement("INSERT INTO Adress(username,type,voivodeship,city,zipcode,street,number) VALUES(?, ?, ?, ?, ?, ?, ?)");
		getAdressesStmt = connection.prepareStatement("SELECT id, username, type, voivodeship, city, zipcode, street, number FROM Adress WHERE username=?");
		checkAdressStmt = connection.prepareStatement("SELECT username, type FROM Adress WHERE username=? AND type=?");
		editAdressStmt = connection.prepareStatement("UPDATE Adress SET voivodeship=?,city=?,zipcode=?,street=?,number=? WHERE username=? AND type=?");
		getAdressStmt = connection.prepareStatement("SELECT voivodeship,city,zipcode,street,number FROM Adress WHERE username=? AND type=?");
		deleteAdressStmt = connection.prepareStatement("DELETE FROM Adress WHERE username=? AND type=?");
		
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void adddb(User user){
		int count = 0;
		try {
			addUserStmt.setString(1, user.getUsername());
			addUserStmt.setString(2, user.getPassword());
			addUserStmt.setString(3, user.getEmail());
			addUserStmt.setString(4, user.getType());
			
			count = addUserStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void addAdress(Adress adress){
		
		int count = 0;
		try{
		addAdressStmt.setString(1, adress.getUsername());
		addAdressStmt.setString(2, adress.getType());
		addAdressStmt.setString(3, adress.getVoivodeship());
		addAdressStmt.setString(4, adress.getCity());
		addAdressStmt.setString(5, adress.getZipcode());
		addAdressStmt.setString(6, adress.getStreet());
		addAdressStmt.setString(7, adress.getNumber());
		
		count = addAdressStmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
	public int deleteAdress(String username,String type){
		int count = 0;
		
		try{
			deleteAdressStmt.setString(1, username);
			deleteAdressStmt.setString(2, type);
			
			count = deleteAdressStmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return count;
	}
	
	public boolean checkUsername(String username){
		
		boolean is = false;
		
		try{
			searchUsernameStmt.setString(1, username);
			
			ResultSet rs = searchUsernameStmt.executeQuery();
			
			if(rs.next()) is = true;
			else is = false;
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return is;
	}
	
	public boolean checkAdress(String username, String type){
		
		boolean is = false;
		
		try{
			checkAdressStmt.setString(1, username);
			checkAdressStmt.setString(2, type);
			
			ResultSet rs = checkAdressStmt.executeQuery();
			
			if(rs.next()) is = true;
				else is = false;
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return is;
	}
	
	public String checkType(String username){
		String type = null;
		
		try{
			getTypeStmt.setString(1, username);
			
			ResultSet rs = getTypeStmt.executeQuery();
			
			if(rs.next()){
				type=rs.getString("type");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return type;
	}
	
	public String checkPassword(String username){
		
		String password = "";
		
		try{
			searchPasswordStmt.setString(1, username);
			
			ResultSet rs = searchPasswordStmt.executeQuery();
			
			if(rs.next()){
				password = rs.getString("password");
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return password;
	}
	
	public void grantPremium(String username){
		int count;
		try{
			grantPremiumStmt.setString(1, username);
			
			count = grantPremiumStmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
	public int editAdress(String username, String type, String voiv,String city, String zip,String street, String number){
		int count = 0;
		
		try{
			editAdressStmt.setString(1, voiv);
			editAdressStmt.setString(2, city);
			editAdressStmt.setString(3, zip);
			editAdressStmt.setString(4, street);
			editAdressStmt.setString(5, number);
			editAdressStmt.setString(6, username);
			editAdressStmt.setString(7, type);
			
			count = editAdressStmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return count;
	}
	
	public Connection getConnection(){
		return connection;
	}
	
	
	public User getProfile(String username){
		
		User u = new User();
		
		try{
			getProfileStmt.setString(1, username);
			ResultSet rs = getProfileStmt.executeQuery();
			
			if(rs.next()){
				u.setId(rs.getInt("id"));
				u.setUsername(rs.getString("username"));
				u.setPassword(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setType(rs.getString("type"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return u;
	}
	
	public Adress getAdress(String username, String type){
		
		Adress a = new Adress();
		
		try{
			getAdressStmt.setString(1, username);
			getAdressStmt.setString(2, type);
			
			ResultSet rs = getAdressStmt.executeQuery();
			
			if(rs.next()){
				a.setVoivodeship(rs.getString("voivodeship"));
				a.setCity(rs.getString("city"));
				a.setZipcode(rs.getString("zipcode"));
				a.setStreet(rs.getString("street"));
				a.setNumber(rs.getString("number"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return a;
	}
	
	public List<Adress> getAdresses(String username){
	
		ad.clear();
		try{
			getAdressesStmt.setString(1, username);
			
			ResultSet rs = getAdressesStmt.executeQuery();
			
			while(rs.next()){
				Adress a = new Adress();
				a.setId(rs.getInt("id"));
				a.setUsername(rs.getString("username"));
				a.setType(rs.getString("type"));
				a.setVoivodeship(rs.getString("voivodeship"));
				a.setCity(rs.getString("city"));
				a.setZipcode(rs.getString("zipcode"));
				a.setStreet(rs.getString("street"));
				a.setNumber(rs.getString("number"));
				ad.add(a);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return ad;
	}
	
	public List<User> getAllUsers() {
		
		
		db.clear();
		try{
			ResultSet rs = getAllUsersStmt.executeQuery();
			
			while(rs.next()){
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setUsername(rs.getString("username"));
				u.setPassword(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setType(rs.getString("type"));
				db.add(u);
			} 
		} catch(SQLException e){
			e.printStackTrace();
		}
		return db;
	}
	
	public int getSize(){
		return db.size();
	}
}