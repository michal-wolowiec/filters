package com.example.filter.domain;

public class Adress {
	
	private int id;
	private String username;
	private String type;
	private String voivodeship;
	private String city;
	private String zipcode;
	private String street;
	private String number;
	
	public Adress(){
		super();
	}
	
	public Adress(String username, String type, String voivodeship, String city, String zipcode, String street, String number){
		
		this.username = username;
		this.type = type;
		this.voivodeship = voivodeship;
		this.city = city;
		this.zipcode = zipcode;
		this.street = street;
		this.number = number;
		
	}
	
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getVoivodeship() {
		return voivodeship;
	}
	public void setVoivodeship(String voivodeship) {
		this.voivodeship = voivodeship;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
}
