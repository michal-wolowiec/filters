package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;
import com.example.filter.domain.User;

public class Data extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		StorageService service = new StorageService();
		
		service.connect();
		
		if(new String(request.getParameter("password")).equals(request.getParameter("cpassword"))){
			
			service.adddb(new User(request.getParameter("username"),request.getParameter("password"),request.getParameter("email"),"normal"));
			out.println("<html><body><h2>Your data</h2>" +
				"<p>Username: " + request.getParameter("username") + "<br />" +
				"<p>Password: " + request.getParameter("password") + "<br />" +
				"<p>Email: " + request.getParameter("email") + "<br />" +
				"<p><a href='index.jsp'>Main Page</a></p>" +
				"</body></html>");
			out.close();
		}else{
			out.println("<html><body><p><a href='register'>" + 
						"Your passwords do not match please fill the form again" + 
						"</a></p></body></html>");
			out.close();
		}
		
	}

}
