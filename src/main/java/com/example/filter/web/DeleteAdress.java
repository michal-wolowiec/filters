package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;

public class DeleteAdress extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		StorageService service = new StorageService();
		
		String username = request.getSession().getAttribute("login").toString();
		
		service.connect();
		
		if(service.deleteAdress(username, request.getParameter("type")) == 1){
			out.println("<html><body>" +
					"<h2>Your '" + request.getParameter("type") + "' Adress has been deleted!!</h2>" +
					"<p><a href='profile'>Back To Your Profile</a></p>" +
					"</body></html>");
		}else
		{
			out.println("<html><body>" +
					"<h2>You can't delete an adress you don't have!!</h2>" +
					"<p><a href='profile'>Back To Your Profile</a></p>" +
					"</body></html>");
		}
	}

}
