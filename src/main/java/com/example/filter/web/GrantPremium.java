package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;


public class GrantPremium extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		StorageService service = new StorageService();
		service.connect();
		
		PrintWriter out = response.getWriter();
		
		if(new String(service.checkType(request.getParameter("username"))).equals("normal") ){
		if(service.checkUsername(request.getParameter("username"))){
			
			out.println("<html><body>" +
					"<p>User: " + request.getParameter("username") + " now has a Premium Status</p>" +
					"<p><a href='index.jsp'>Main Page</a></p>" +
					"</body></html>");
			service.grantPremium(request.getParameter("username"));
		}else{
			out.println("<html><body>" +
						"<h2>There is no User Named: " + request.getParameter("username") + "</h2>" +
						"<p><a href='index.jsp'>Main Page</a></p>" +
						"</body></html>");
		}
		
		}else{
			out.println("<html><body>" +
					"<h2>User: " + request.getParameter("username") + " Already Has A Premium Status</h2>" +
					"<p><a href='index.jsp'>Main Page</a></p>" +
					"</body></html>");
		}
		
	}

}
