package com.example.filter.web.filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;


public class GrantPremiumFilter implements Filter {



	public void destroy() {
		// TODO Auto-generated method stub
	}


	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		res.setContentType("text/html");
		
		PrintWriter out = res.getWriter();
		
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
		StorageService service = new StorageService();
		service.connect();
        
		String sesusername = request.getSession().getAttribute("login").toString();
		
		
		if(request.getSession().getAttribute("login") == "anonymous"){
			out.println("<html><body>" +
					"<h2>Access Denied</h2>" +
					"<p><a href='index.jsp'>Main Page</a></p>" +
					"</body></html>");
		}else{
		if(new String(service.checkType(sesusername)).equals("admin") ){
		chain.doFilter(req, res);
		}else{
			out.println("<html><body>" +
						"<h2>Access Denied</h2>" +
						"<p><a href='index.jsp'>Main Page</a></p>" +
						"</body></html>");
			}
		}
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
