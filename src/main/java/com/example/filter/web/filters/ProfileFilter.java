package com.example.filter.web.filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ProfileFilter implements Filter {



	public void destroy() {
		// TODO Auto-generated method stub
	}


	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        PrintWriter out = response.getWriter();
        
        if(new String(request.getSession().getAttribute("login").toString()).equals("anonymous")){
    		out.println("<html><body>"+
    					"<h2>You are not logged in!!</h2>" +
    					"<p><a href='loginform'>Login</a></p>" +
    					"<p><a href='index.jsp'>Main Page</a></p>");
            }else{   
            	chain.doFilter(req, res);
            }
		
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
