package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;
import com.example.filter.domain.User;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		StorageService service = new StorageService();
		
		PrintWriter out = response.getWriter();
		
		service.connect();
		
		if(service.checkUsername(request.getParameter("username"))){
			if(new String(service.checkPassword(request.getParameter("username"))).equals(request.getParameter("password"))){
				request.getSession().setAttribute("login", request.getParameter("username"));
				out.println("<html><body>" +
						"<p>You have logged in succesfuly</p>" + 
						"<p><a href='profile'>Your Profile</a></p>");
				request.getSession().setAttribute("login", request.getParameter("username"));
			}else{
				out.println("<html><body>" +
						"<h2>You have entered a wrong password</h2>" + 
						"<p><a href='loginform'>Login Again</a></p>");
			}
		}else{
			out.println("<html><body>" +
						"<h2>User do not exist in the database</h2>" + 
						"<p><a href='index.jsp'>Main Page</a></p>");
		}
		
		
		
		
		}
		
	}

