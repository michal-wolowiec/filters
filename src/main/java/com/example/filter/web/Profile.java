package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;
import com.example.filter.domain.Adress;
import com.example.filter.domain.User;


public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
	
		
		StorageService service = new StorageService();
		
		service.connect();
		
		String username = request.getSession().getAttribute("login").toString();
		
		User u = service.getProfile(request.getSession().getAttribute("login").toString());
	
		
		
		PrintWriter out = response.getWriter();
		
		out.println("<html><body>" +
					"<h2>Your Profile</h2>" + 
					"<p>Username: " + u.getUsername() + "</p>" +
					"<p>Password: " + u.getPassword() + "</p>" +
					"<p>Email: " + u.getEmail() + "</p>" +
					"<p>Account Type: " + u.getType() + "</p>" +
					"<p><a href='index.jsp'>Main Page</a>****<a href='logout'>Logout</a></p>" +
					"<p>***********************************</p>");
		
		for (Adress adress : service.getAdresses(username)) {
			  out.println("<p>Type: " + adress.getType() + "</p>" + 
					  "<p>Voivodeship: " + adress.getVoivodeship() + "</p>" +
					  "<p>City: " + adress.getCity() + "</p>" +
					  "<p>Zip Code: " + adress.getZipcode() + "</p>" +
					  "<p>Street: " + adress.getStreet() + "</p>" +
					  "<p>No of House/Appartement: " + adress.getNumber() + "</p>");
					  switch(adress.getType()){
					  	case "Work":
						  out.println("<p><a href='editadressform?type=Work'>Edit</a>****<a href='deleteadressform?type=Work'>Delete</a></p>" +
								  "<p>---------------------------------</p>");
						  break;
					  	case "Home":
					  		out.println("<p><a href='editadressform?type=Home'>Edit</a>****<a href='deleteadressform?type=Home'>Delete</a></p>" +
									  "<p>---------------------------------</p>");
					  		break;
					  	case "Of Correspondence":
					  		out.println("<p><a href='editadressform?type=Of+Correspondence'>Edit</a>****<a href='deleteadressform?type=Of+Correspondence'>Delete</a></p>" +
									  "<p>---------------------------------</p>");
					  		break;
					  }
					  
		  }
		
		out.println("<p><a href='adressform'>Add Adress</a></p>");
	}

}
