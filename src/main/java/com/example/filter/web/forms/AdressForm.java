package com.example.filter.web.forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdressForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		
		PrintWriter out = response.getWriter();
		
		out.println("<h2>Add an Adress</h2>" +
				"<form action='addadress'>" +
				"Type Of Adress: <select name='type'>" +
				"<option>Work</option>" + 
				"<option>Home</option>" +
				"<option>Of Correspondence</option>" + 
				"</select> <br />" +
				"Voivodeship:<select name='voiv'>" +
				"<option>Dolnoslaskie</option>" + 
				"<option>Kujawsko-Pomorskie</option>" +
				"<option>Lubeskie</option>" +
				"<option>Lubuskie</option>" +
				"<option>Lodzkie</option>" +
				"<option>Malopolskie</option>" +
				"<option>Mazowieckie</option>" +
				"<option>Opolskie</option>" +
				"<option>Podkarpackie</option>" +
				"<option>Podlaskie</option>" +
				"<option>Pomorskie</option>" +
				"<option>Slaskie</option>" +
				"<option>Swietokrzyskie</option>" +
				"<option>Warminsko-Mazurskie</option>" +
				"<option>Wielkopolskie</option>" +
				"<option>Zachodniopomorskie</option>" +
				"</select> <br />" +
				"City: <input type='text' name='city'  required><br />" +
				"Zip Code: <input type='text' name='zip'> <br />" +
				"Street: <input type='text' name='street'> <br />" +
				"No. Of House/Appartement:<input type='text' name='number'  required><br />" +
				"<input type='submit' value=' OK ' />" +
				"</form>");
		
	}


}
