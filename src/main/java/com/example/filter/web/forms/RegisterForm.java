package com.example.filter.web.forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegisterForm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html>"+
					"<head><title>Register</title></head>" +
					"<body><h2>Register A New User</h2>" +
					"<form action='data'>" + 
					"Username: <input type='text' name='username' /> <br />" +
					"Password: <input type='password' name='password' /> <br />" +
					"Confirm Password: <input type='password' name='cpassword' /> <br />" +
					"Email: <input type='text' name='email' /> <br />" + 
					"<input type='submit' value=' OK ' />" +
					"</form>" +
					"<p><a href='index.jsp'>Main Page</a></p>" +
					"</body></html>");
		out.close();
	}

}
