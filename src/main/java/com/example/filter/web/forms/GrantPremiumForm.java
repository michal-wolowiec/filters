package com.example.filter.web.forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GrantPremiumForm extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		
		PrintWriter out = response.getWriter();
		out.println("<html>"+
					"<head><title>Grant Someone Premium Status</title></head>" +
					"<body><h2>Login</h2>" +
					"<form action='grantpremium'>" + 
					"Username: <input type='text' name='username' /> <br />" +
					"<input type='submit' value=' GRANT ' />" +
					"</form>" +
					"</body></html>");
		out.close();
	}

}
