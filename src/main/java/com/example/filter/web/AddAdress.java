package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;
import com.example.filter.domain.Adress;


public class AddAdress extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		StorageService service = new StorageService();
		
		String username = request.getSession().getAttribute("login").toString();
		
		service.connect();
		
		if(!service.checkAdress(username, request.getParameter("type"))){
		service.addAdress(new Adress(username, request.getParameter("type"), request.getParameter("voiv"), request.getParameter("city"), request.getParameter("zip"), request.getParameter("street"), request.getParameter("number")));
		out.println("<html><body><h2>The following Adress was added to the Database</h2>" +
			"<p>Type: " + request.getParameter("type") + "<br />" +
			"<p>Voivodeship: " + request.getParameter("voiv") + "<br />" +
			"<p>City: " + request.getParameter("city") + "<br />" +
			"<p>Zip Code: " + request.getParameter("zip") + "<br />" +
			"<p>Street: " + request.getParameter("street") + "<br />" +
			"<p>Number of House/Appartement: " + request.getParameter("number") + "<br />" +
			"<p><a href='profile'>Your Profile</a></p>" +
			"</body></html>");
		}else{
			out.println("<html><body>" +
						"<h2>User: '" + username + "' Already has " + request.getParameter("type") + " Adress!!</h2>" +
						"<p><a href='index.jsp'>Main Page</a></p>" +
						"</body></html>");
		}
	}

}
