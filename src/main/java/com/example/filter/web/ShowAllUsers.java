package com.example.filter.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.filter.service.StorageService;
import com.example.filter.domain.User;




public class ShowAllUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		
		StorageService service = new StorageService();
		service.connect();
		
		PrintWriter out = response.getWriter();
		
		for (User user : service.getAllUsers()) {
			  out.println("<p>Username: " + user.getUsername() + "</p>" + 
					  "<p>Email: " + user.getEmail() + "</p>" +
					  "<p>Account Type: " + user.getType() + "</p>" +
					  "<p>**********************************</p>");
		  }
	}

}
